package id.geomedia.apps.ideeverywhere.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.ArrayList;

import id.geomedia.apps.ideeverywhere.R;
import id.geomedia.apps.ideeverywhere.activity.resto.RestoActivity;
import id.geomedia.apps.ideeverywhere.activity.resto.RestoDetailActivity;
import id.geomedia.apps.ideeverywhere.fragment.services.Resto.MenuGridviewAdapter;
import id.geomedia.apps.ideeverywhere.fragment.services.Resto.MenuListGrid;
import id.geomedia.apps.ideeverywhere.fragment.services.Resto.MenulistBeanclass;
import id.geomedia.apps.ideeverywhere.fragment.services.Resto.MenulistGridviewAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class CityGuidesFragment extends Fragment {

    private AdView mAdView;
    private GridView gridview;
    private ArrayList<MenuListGrid> beanclassArrayList;
    private MenuGridviewAdapter gridviewAdapter;

    private int[] img_m1 = {R.drawable.no_image, R.drawable.no_image, R.drawable.no_image, R.drawable.no_image, R.drawable.no_image, R.drawable.no_image, R.drawable.no_image, R.drawable.no_image};
    private String[] item_name = {"International & Video Call", "Maps", "City Guides", "List of Event & Activity", "Transportation", "Flight", "Hotel & Accommodation", "Nightlife & Lifestyle","Restaurant & Cafe","Diving Guide"};
    //private String[] description_m1 = {"Freshly brewed coffee.", "Hearty, hot & full of flavour.", "Perfectly baked goodies.", "Fresh, healthy and tasty.","Freshly brewed coffee.", "Hearty, hot & full of flavour.", "Perfectly baked goodies.", "Fresh, healthy and tasty."};


    public CityGuidesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_city_guides, container, false);
        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        //MobileAds.initialize(getContext(), "ca-app-pub-5600430177489071~8231229538");
        MobileAds.initialize(getContext(), "ca-app-pub-3940256099942544/6300978111");
        AdView adView = new AdView(getContext());
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");

        mAdView = v.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        gridview = v.findViewById(R.id.gridview_cityguides);
        beanclassArrayList = new ArrayList<MenuListGrid>();

        for (int i = 0; i < img_m1.length; i++) {

            MenuListGrid beanclass = new MenuListGrid(img_m1[i], item_name[i]);
            beanclassArrayList.add(beanclass);

        }
        gridviewAdapter = new MenuGridviewAdapter(getContext(), beanclassArrayList);

        gridview.setAdapter(gridviewAdapter);
        /*gridview.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent myIntent = new Intent(getContext(), RestoDetailActivity.class);
                startActivity(myIntent);

            }
        });*/
        return v;
    }

}
