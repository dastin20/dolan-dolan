package id.geomedia.apps.ideeverywhere.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;
import java.util.List;

import id.geomedia.apps.ideeverywhere.R;
import id.geomedia.apps.ideeverywhere.application.AppController;
import id.geomedia.apps.ideeverywhere.application.URLs;
import id.geomedia.apps.ideeverywhere.fragment.services.ServiceAllFragment;
import id.geomedia.apps.ideeverywhere.fragment.services.ServiceChatFragment;
import id.geomedia.apps.ideeverywhere.fragment.services.ServiceFnbFragment;
import id.geomedia.apps.ideeverywhere.fragment.services.ServiceHouseFragment;
import id.geomedia.apps.ideeverywhere.fragment.services.ServiceReceptionFragment;
import id.geomedia.apps.ideeverywhere.fragment.services.ServiceRoomFragment;
import id.geomedia.apps.ideeverywhere.fragment.services.ServiceSpaFragment;
import id.geomedia.apps.ideeverywhere.model.CountryModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceFragment extends Fragment {
    private static final String TAG = ServiceFragment.class.getSimpleName();
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ProgressDialog pDialog;

    public ServiceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_service, container, false);
        viewPager = v.findViewById(R.id.viewpager_service);
        setupViewPager(viewPager);

        tabLayout = v.findViewById(R.id.tab_service);
        tabLayout.setupWithViewPager(viewPager);
        return v;
    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());

        adapter.addFragment(new ServiceAllFragment(), "All");
        adapter.addFragment(new ServiceFnbFragment(), "F&B");
        adapter.addFragment(new ServiceSpaFragment(), "Spa");
        adapter.addFragment(new ServiceReceptionFragment(), "Receptionist");
        adapter.addFragment(new ServiceRoomFragment(), "Room Service");
        adapter.addFragment(new ServiceHouseFragment(), "Housekeeping");
        adapter.addFragment(new ServiceChatFragment(), "Chat Officer");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

}
