package id.geomedia.apps.ideeverywhere.adapter;

/**
 * Created by Lenovo on 5/16/2018.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.bumptech.glide.Glide;

import java.util.List;

import id.geomedia.apps.ideeverywhere.R;
import id.geomedia.apps.ideeverywhere.application.AppController;
import id.geomedia.apps.ideeverywhere.application.URLs;
import id.geomedia.apps.ideeverywhere.model.FoodModel;

import static android.widget.ImageView.ScaleType.CENTER_CROP;

public class HotelServiceAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<FoodModel> fItems;

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    public HotelServiceAdapter(Activity activity, List<FoodModel> countryItems) {
        this.activity = activity;
        this.fItems = countryItems;
    }

    @Override
    public int getCount() {
        return fItems.size();
    }

    @Override
    public Object getItem(int location) {
        return fItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_fnb, null);

        TextView id = convertView.findViewById(R.id.text_id_fnb);
        TextView price = convertView.findViewById(R.id.text_price_fnb);
        TextView name = convertView.findViewById(R.id.text_name_fnb);
        TextView time = convertView.findViewById(R.id.text_servicetime_fnb);

        NetworkImageView images = convertView.findViewById(R.id.img);
        // getting movie data for the row
        FoodModel m = fItems.get(position);
        images.setScaleType(CENTER_CROP);
        images.setImageUrl(m.getThumbnailUrl(), imageLoader);
        id.setText(m.getIdx());
        name.setText(m.getTitle());
        price.setText(m.getPrice());
        time.setText(m.getTimeS());

        return convertView;
    }

}