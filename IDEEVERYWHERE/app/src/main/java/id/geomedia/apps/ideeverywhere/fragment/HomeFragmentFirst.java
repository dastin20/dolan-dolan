package id.geomedia.apps.ideeverywhere.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.balysv.materialripple.MaterialRippleLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.geomedia.apps.ideeverywhere.HomeHotel;
import id.geomedia.apps.ideeverywhere.R;
import id.geomedia.apps.ideeverywhere.activity.Home;
import id.geomedia.apps.ideeverywhere.activity.Walkthrought;
import id.geomedia.apps.ideeverywhere.activity.resto.RestoActivity;
import id.geomedia.apps.ideeverywhere.activity.resto.RestoDetailActivity;
import id.geomedia.apps.ideeverywhere.adapter.AdapterGridSingleLine;
import id.geomedia.apps.ideeverywhere.adapter.GridAdapter;
import id.geomedia.apps.ideeverywhere.adapter.ImageAttractionAdapter;
import id.geomedia.apps.ideeverywhere.adapter.ImageDivingAdapter;
import id.geomedia.apps.ideeverywhere.adapter.ImageSliderAdapter;
import id.geomedia.apps.ideeverywhere.application.AppController;
import id.geomedia.apps.ideeverywhere.application.SessionManager;
import id.geomedia.apps.ideeverywhere.application.URLs;
import id.geomedia.apps.ideeverywhere.data.DataGenerator;
import id.geomedia.apps.ideeverywhere.data.SQLiteHandler;
import id.geomedia.apps.ideeverywhere.fragment.ProvinceFragment;
import id.geomedia.apps.ideeverywhere.model.Image;
import id.geomedia.apps.ideeverywhere.util.Tools;
import id.geomedia.apps.ideeverywhere.widget.SpacingItemDecoration;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragmentFirst extends Fragment {

    private TextView hotelName,btn_destination, btn_places;
    private int dotscount;
    private ImageView[] dots;
    private Toolbar toolbar;
    private View parent_view;
    private ViewPager viewPager,viewPagerAttraction,viewPagerDiving;
    private LinearLayout layout_dots;
    private AdapterImageSlider adapterImageSlider;
    private SQLiteHandler db;
    private SessionManager session;

    RequestQueue rq;

    ImageView headers;
    LinearLayout sliderDotspanel;
    List<Tools> sliderImg;
    ImageSliderAdapter viewPagerAdapter;
    ImageAttractionAdapter viewPagerAttractionAdapter;
    ImageDivingAdapter viewPagerDivAdapter;

    Context context;

    public HomeFragmentFirst() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home_first, container, false);

        context = this.getActivity();
        hotelName = v.findViewById(R.id.txtHotelName);
        headers = v.findViewById(R.id.imgDestination);

        headers.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                /*Intent i = new Intent(Home.this, Home.class);
                startActivity(i);*/
                Toast.makeText(getActivity(),"Destination hotel clicked",Toast.LENGTH_SHORT).show();
                //loadFragment( new ServiceFragment());
                Log.e("Destination","Destination");
            }
        });

        // SqLite database handler
        db = new SQLiteHandler(context);
        // session manager
        session = new SessionManager(context);
        // Fetching user details from sqlite
        HashMap<String, String> user = db.getHotelDetails();
        String name_of_hotel = user.get("name");
//        Intent intent = getIntent();
//        String names = intent.getStringExtra("HotelName");
        hotelName.setText(name_of_hotel);

        viewPager = (ViewPager) v.findViewById(R.id.pager_accomodation);
        sliderDotspanel = (LinearLayout) v.findViewById(R.id.SliderDots);

        initComponentAccomodation();

        return v;
    }

    private void initInstances() {

    }

    private void initComponentAccomodation() {
        rq = AppController.getInstance().getRequestQueue();
        sliderImg = new ArrayList<>();
        //layout_dots = (LinearLayout) findViewById(R.id.layout_dots1);

        sendRequestAccomodation();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for(int i = 0; i< dotscount; i++){
                    dots[i].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.inactive_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void sendRequestAccomodation(){

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest( URLs.URL_IMAGE_SLIDER, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.e("RESPONSE",response.toString());
                for(int i = 0; i < response.length(); i++){
                    Tools sliderUtils = new Tools();
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);

                        sliderUtils.setSliderImageUrl(URLs.ASSETS_IMG_ACCOMODATION + jsonObject.getString("image_url"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    sliderImg.add(sliderUtils);

                }

                viewPagerAdapter = new ImageSliderAdapter(sliderImg, getActivity());

                viewPager.setAdapter(viewPagerAdapter);

                dotscount = viewPagerAdapter.getCount();
                dots = new ImageView[dotscount];

                for(int i = 0; i < dotscount; i++){

                    dots[i] = new ImageView(getActivity());
                    dots[i].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.inactive_dot));

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    params.setMargins(8, 0, 8, 0);

                    sliderDotspanel.addView(dots[i], params);

                }

                dots[0].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.active_dot));
               // initComponentAttraction();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("volley", "error : " + error.getMessage());
            }
        });

        AppController.getInstance().addToRequestQueue(jsonArrayRequest);

    }

//    private void initComponentAttraction() {
//        rq = AppController.getInstance().getRequestQueue();
//        sliderImg = new ArrayList<>();
//        //layout_dots = (LinearLayout) findViewById(R.id.layout_dots1);
//        viewPager = (ViewPager) findViewById(R.id.pager2);
//        sliderDotspanel = (LinearLayout) findViewById(R.id.SliderDots2);
//
//        sendRequestAttraction();
//
//        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//                for(int i = 0; i< dotscount; i++){
//                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.inactive_dot));
//                }
//
//                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
//    }
//
//    public void sendRequestAttraction(){
//
//        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest( URLs.URL_IMAGE_SLIDER_ATTRACTION, new Response.Listener<JSONArray>() {
//            @Override
//            public void onResponse(JSONArray response) {
//
//                for(int i = 0; i < response.length(); i++){
//                    Tools sliderUtils = new Tools();
//                    try {
//                        JSONObject jsonObject = response.getJSONObject(i);
//
//                        sliderUtils.setSliderImageUrl(URLs.ASSETS_IMG_ATTRACTION + jsonObject.getString("image_url"));
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                    sliderImg.add(sliderUtils);
//
//                }
//
//                viewPagerAttractionAdapter = new ImageAttractionAdapter(sliderImg, HomeHotel.this);
//
//                viewPager.setAdapter(viewPagerAttractionAdapter);
//
//                dotscount = viewPagerAttractionAdapter.getCount();
//                dots = new ImageView[dotscount];
//
//                for(int i = 0; i < dotscount; i++){
//
//                    dots[i] = new ImageView(HomeHotel.this);
//                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.inactive_dot));
//
//                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//
//                    params.setMargins(8, 0, 8, 0);
//
//                    sliderDotspanel.addView(dots[i], params);
//
//                }
//
//                dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));
//                initComponentDiving();
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                Log.d("volley", "error : " + error.getMessage());
//            }
//        });
//
//        AppController.getInstance().addToRequestQueue(jsonArrayRequest);
//
//    }
//
//
//    private void initComponentDiving() {
//        rq = AppController.getInstance().getRequestQueue();
//        sliderImg = new ArrayList<>();
//        //layout_dots = (LinearLayout) findViewById(R.id.layout_dots1);
//        viewPager = (ViewPager) findViewById(R.id.pager3);
//        sliderDotspanel = (LinearLayout) findViewById(R.id.SliderDots3);
//
//        sendRequestDiving();
//
//        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//                for(int i = 0; i< dotscount; i++){
//                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.inactive_dot));
//                }
//
//                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
//    }
//
//    public void sendRequestDiving(){
//
//        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest( URLs.URL_IMAGE_SLIDER_DIVING, new Response.Listener<JSONArray>() {
//            @Override
//            public void onResponse(JSONArray response) {
//
//                for(int i = 0; i < response.length(); i++){
//                    Tools sliderUtils = new Tools();
//                    try {
//                        JSONObject jsonObject = response.getJSONObject(i);
//
//                        sliderUtils.setSliderImageUrl(URLs.ASSETS_IMG_DIVSPOT + jsonObject.getString("image_url"));
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                    sliderImg.add(sliderUtils);
//
//                }
//
//                viewPagerDivAdapter = new ImageDivingAdapter(sliderImg, HomeHotel.this);
//
//                viewPager.setAdapter(viewPagerDivAdapter);
//
//                dotscount = viewPagerDivAdapter.getCount();
//                dots = new ImageView[dotscount];
//
//                for(int i = 0; i < dotscount; i++){
//
//                    dots[i] = new ImageView(HomeHotel.this);
//                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.inactive_dot));
//
//                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//
//                    params.setMargins(8, 0, 8, 0);
//
//                    sliderDotspanel.addView(dots[i], params);
//
//                }
//
//                dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                Log.d("volley", "error : " + error.getMessage());
//            }
//        });
//
//        AppController.getInstance().addToRequestQueue(jsonArrayRequest);
//
//    }

    private static class AdapterImageSlider extends PagerAdapter {

        private Activity act;
        private List<Image> items;

        private OnItemClickListener onItemClickListener;

        private interface OnItemClickListener {
            void onItemClick(View view, Image obj);
        }

        public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
            this.onItemClickListener = onItemClickListener;
        }

        // constructor
        private AdapterImageSlider(Activity activity, List<Image> items) {
            this.act = activity;
            this.items = items;
        }

        @Override
        public int getCount() {
            return this.items.size();
        }

        public Image getItem(int pos) {
            return items.get(pos);
        }

        public void setItems(List<Image> items) {
            this.items = items;
            notifyDataSetChanged();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((RelativeLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            final Image o = items.get(position);
            LayoutInflater inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.item_slider_image, container, false);

            ImageView image = (ImageView) v.findViewById(R.id.image);
            MaterialRippleLayout lyt_parent = (MaterialRippleLayout) v.findViewById(R.id.lyt_parent);
            Tools.displayImageOriginal(act, image, o.image);
            lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(v, o);
                    }
                }
            });

            ((ViewPager) container).addView(v);

            return v;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((RelativeLayout) object);

        }

    }

}
