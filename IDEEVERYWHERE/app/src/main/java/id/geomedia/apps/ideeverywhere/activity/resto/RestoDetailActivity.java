package id.geomedia.apps.ideeverywhere.activity.resto;

import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;

import customView.ExpandableHeightListView;
import id.geomedia.apps.ideeverywhere.R;
import id.geomedia.apps.ideeverywhere.fragment.services.Resto.Menu2BeanclassList;
import id.geomedia.apps.ideeverywhere.fragment.services.Resto.Menu2ListviewAdapter;

public class RestoDetailActivity extends AppCompatActivity {

    private ExpandableHeightListView listview;
    private ArrayList<Menu2BeanclassList> Bean;
    private Menu2ListviewAdapter baseAdapter;

    TextView cutprice;

    private int[] image_m2 = {R.drawable.img1_m2, R.drawable.img2_m2, R.drawable.img3_m2, R.drawable.img3_m3, R.drawable.img1_m2, R.drawable.img2_m2};
    private String[] item_name_m2 = {"Choco Frappe", "Choco Frappe", "Choco Frappe", "Choco Frappe","Choco Frappe", "Choco Frappe"};
    private String[] text_m2 = {"Chocolate Whirl", "Chocolate Whirl", "Chocolate Whirl", "Chocolate Whirl", "Chocolate Whirl","Chocolate Whirl",};
    private String[] price_m2 = {"$ 9", "$ 11.5", "$ 11.5", "$ 15.5", "$ 4.5", "$ 9.5"};
    private String[] ratingtext_m2 = {"3,200", "3,200", "3,030", "3,000","2,400", "1,200"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resto_detail);

        cutprice = (TextView) findViewById(R.id.cutprice);

        cutprice.setPaintFlags(cutprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        //        /        ********LISTVIEW***********


        listview = (ExpandableHeightListView)findViewById(R.id.listview);


        Bean = new ArrayList<Menu2BeanclassList>();

        for (int i= 0; i< image_m2.length; i++){

            Menu2BeanclassList BeanclassList = new Menu2BeanclassList(image_m2[i], item_name_m2[i], text_m2[i], price_m2[i], ratingtext_m2[i]);
            Bean.add(BeanclassList);

        }

        baseAdapter = new Menu2ListviewAdapter(this, RestoDetailActivity.this, Bean) {
        };

        listview.setAdapter(baseAdapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent myIntent = new Intent(RestoDetailActivity.this, OrderDetail.class);
                startActivity(myIntent);

            }
        });
    }
}
