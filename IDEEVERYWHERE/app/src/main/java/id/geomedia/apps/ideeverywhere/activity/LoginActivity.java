package id.geomedia.apps.ideeverywhere.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import id.geomedia.apps.ideeverywhere.HomeHotel;
import id.geomedia.apps.ideeverywhere.R;
import id.geomedia.apps.ideeverywhere.application.AppController;
import id.geomedia.apps.ideeverywhere.application.SessionManager;
import id.geomedia.apps.ideeverywhere.application.URLs;
import id.geomedia.apps.ideeverywhere.data.SQLiteHandler;
import id.geomedia.apps.ideeverywhere.util.Tools;

public class LoginActivity extends AppCompatActivity {

    //private ProgressBar progress_bar;
    private Button login;
    private TextInputEditText inputPassword;

    private SQLiteHandler db;
    private SessionManager session;
    private ProgressDialog pDialog;

    private static final String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        inputPassword = findViewById(R.id.t_password);
        login = findViewById(R.id.login);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Session manager
        session = new SessionManager(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity

                Intent intent = new Intent(LoginActivity.this, HomeHotel.class);
                startActivity(intent);
                finish();

        }
        //Tools.setSystemBarColor(this, R.color.cyan_800);


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                onAction();
            }
        });
    }

    private void onAction() {

        String password = inputPassword.getText().toString().trim();

        // Check for empty data in the form
        if (!password.isEmpty()) {
            // login user
            checkLogin(password);
        } else {
            // Prompt user to enter credentials
            Toast.makeText(getApplicationContext(),
                    "Please enter the passkey!", Toast.LENGTH_LONG)
                    .show();
        }
    }

    /**
     * function to verify login details in mysql db
     */
    private void checkLogin(final String pas) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Logging in ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                URLs.URL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session
                        session.setLogin(true);
                        //String uid = jObj.getString("uid");

                        JSONObject user = jObj.getJSONObject("hotel");
                        String id = user.getString("id");
                        String hid = user.getString("hid");
                        String name = user.getString("name");
                        String room = user.getString("room_num");
                        String address = user.getString("address");
                        String country = user.getString("country");
                        String city = user.getString("city");
                        String images = user.getString("images");
                        db.addHotel(id, name, room, address, city, country);
                        session.setHotel(id,name,room);

                        // Launch main activity
                        Intent intent = new Intent(LoginActivity.this,
                                HomeHotel.class);
                        intent.putExtra("HotelName", name);
                        intent.putExtra("id", id);
                        startActivity(intent);
                        finish();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("pass_key", pas);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
