package id.geomedia.apps.ideeverywhere.activity.resto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import id.geomedia.apps.ideeverywhere.R;

public class OrderDetail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        Spinner dropdown1 = (Spinner) findViewById(R.id.spinner1);

        String[] items1 = new String[]{"1", "2", "3", "4"};
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, R.layout.spinner_item, items1);
        dropdown1.setAdapter(adapter1);
    }
}
