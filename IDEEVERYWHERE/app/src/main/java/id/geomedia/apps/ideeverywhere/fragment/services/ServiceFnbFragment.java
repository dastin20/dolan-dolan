package id.geomedia.apps.ideeverywhere.fragment.services;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.geomedia.apps.ideeverywhere.R;
import id.geomedia.apps.ideeverywhere.adapter.CountryAdapter;
import id.geomedia.apps.ideeverywhere.adapter.HotelServiceAdapter;
import id.geomedia.apps.ideeverywhere.application.AppController;
import id.geomedia.apps.ideeverywhere.application.SessionManager;
import id.geomedia.apps.ideeverywhere.application.URLs;
import id.geomedia.apps.ideeverywhere.data.SQLiteHandler;
import id.geomedia.apps.ideeverywhere.fragment.HomeDestinationFragment;
import id.geomedia.apps.ideeverywhere.fragment.ServiceFragment;
import id.geomedia.apps.ideeverywhere.model.CountryModel;
import id.geomedia.apps.ideeverywhere.model.FoodModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceFnbFragment extends Fragment {

    private HotelServiceAdapter hsAdapter;
    private List<FoodModel> FoodList = new ArrayList<FoodModel>();
    private ProgressDialog pDialog;
    private static final String TAG = ServiceFragment.class.getSimpleName();

    private SessionManager session;
    private SQLiteHandler db;

    public ServiceFnbFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_service_fnb, container, false);
        ListView listView = v.findViewById(R.id.listview_fnb);
        hsAdapter = new HotelServiceAdapter(getActivity(), FoodList);
        listView.setAdapter(hsAdapter);
        // SqLite database handler
        db = new SQLiteHandler(getContext());
        // session manager
        session = new SessionManager(getContext());
        // Fetching user details from sqlite
        HashMap<String, String> user = db.getHotelDetails();
        final String id = user.get("id");

        loadFoodandBeverage(id);
        return v;
    }

    private void loadFoodandBeverage(final String id) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_SERVICE_HOTEL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //converting the string to json array object
                            JSONArray array = new JSONArray(response);

                            //traversing through all the object
                            for (int i = 0; i < array.length(); i++) {

                                //getting product object from json array
                                JSONObject tags = array.getJSONObject(i);

                                //adding the product to product list
                                FoodModel fm = new FoodModel();
                                fm.setId(tags.getInt("id"));
                                fm.setTitle(tags.getString("service"));
                                fm.setPrice(tags.getString("price"));
                                fm.setTimeS(tags.getString("time"));

                                fm.setThumbnailUrl(URLs.URL_SERVER_ASSETS_IMG + tags.getString("image"));
                                // adding movie to movies array
                                FoodList.add(fm);
                            }

                            //creating adapter object and setting it to recyclerview
                            // CountryAdapter adapter = new CountryAdapter(getChildFragmentManager(), CountryList);
                            //adapter = new CountryAdapter(getActivity(), CountryList);

                            hsAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        hidePDialog();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("HotelId", id);
                params.put("Category", "1");

                return params;
            }

        };

        //adding our stringrequest to queue
        //Volley.newRequestQueue(getContext()).add(stringRequest);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

}
