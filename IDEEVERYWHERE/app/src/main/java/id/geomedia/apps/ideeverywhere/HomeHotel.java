package id.geomedia.apps.ideeverywhere;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.balysv.materialripple.MaterialRippleLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.geomedia.apps.ideeverywhere.activity.Home;
import id.geomedia.apps.ideeverywhere.activity.LoginActivity;
import id.geomedia.apps.ideeverywhere.adapter.ImageAttractionAdapter;
import id.geomedia.apps.ideeverywhere.adapter.ImageDivingAdapter;
import id.geomedia.apps.ideeverywhere.adapter.ImageSliderAdapter;
import id.geomedia.apps.ideeverywhere.application.AppController;
import id.geomedia.apps.ideeverywhere.application.SessionManager;
import id.geomedia.apps.ideeverywhere.application.URLs;
import id.geomedia.apps.ideeverywhere.data.SQLiteHandler;
import id.geomedia.apps.ideeverywhere.fragment.ServiceFragment;
import id.geomedia.apps.ideeverywhere.fragment.HomeFragmentFirst;
import id.geomedia.apps.ideeverywhere.model.Image;
import id.geomedia.apps.ideeverywhere.util.Tools;
import id.geomedia.apps.ideeverywhere.util.ViewAnimation;

public class HomeHotel extends AppCompatActivity {
    private TextView hotelName;
    private Toolbar toolbar;
    private SQLiteHandler db;
    private SessionManager session;

    ImageView headers;


    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_hotel);
        hotelName = findViewById(R.id.txtHotelName);
        headers = findViewById(R.id.imgDestination);

        headers.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                /*Intent i = new Intent(Home.this, Home.class);
                startActivity(i);*/

                loadFragment( new ServiceFragment());
            }
        });
        // SqLite database handler
        db = new SQLiteHandler(this);
        // session manager
        session = new SessionManager(this);
        // Fetching user details from sqlite
        HashMap<String, String> user = db.getHotelDetails();
        String name_of_hotel = user.get("name");
//        Intent intent = getIntent();
//        String names = intent.getStringExtra("HotelName");
        hotelName.setText(name_of_hotel);

        toolbar = findViewById(R.id.toolbar_Apps);

        //initComponentAccomodation();

        loadFragment( new HomeFragmentFirst());

    }

    private void loadFragment(Fragment fragment) {
        // load fragment
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.frame_content, fragment);
//        transaction.addToBackStack(null);
//        transaction.commit();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame_content, fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

}
