package id.geomedia.apps.ideeverywhere.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import id.geomedia.apps.ideeverywhere.BottomNavigationBehavior;
import id.geomedia.apps.ideeverywhere.R;
import id.geomedia.apps.ideeverywhere.fragment.AccountFragment;
import id.geomedia.apps.ideeverywhere.fragment.AssistanceFragment;
import id.geomedia.apps.ideeverywhere.fragment.CityGuidesFragment;
import id.geomedia.apps.ideeverywhere.fragment.HomeDestinationFragment;
import id.geomedia.apps.ideeverywhere.fragment.HomeFragment;
import id.geomedia.apps.ideeverywhere.fragment.HomePlacesFragment;
import id.geomedia.apps.ideeverywhere.fragment.SalePromoFragment;
import id.geomedia.apps.ideeverywhere.fragment.ServiceFragment;

public class Home extends AppCompatActivity {


    ImageView headers;
TextView txtHotelName;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        /* coba coba git from dastin */
        /* OK brodd */
        headers = findViewById(R.id.imgDestination);

        headers.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                /*Intent i = new Intent(Home.this, Home.class);
                startActivity(i);*/

                loadFragment( new ServiceFragment());
            }
        });

        txtHotelName = findViewById(R.id.txtHotelName);

        txtHotelName.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent i = new Intent(Home.this, Home.class);
                startActivity(i);

               // loadFragment( new ServiceFragment());
            }
        });



        BottomNavigationView bottomNavigationView =
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) bottomNavigationView.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationBehavior());

        BottomNavigationMenuView menuView = (BottomNavigationMenuView)
                bottomNavigationView.getChildAt(0);

        try {

            Field shiftingMode = menuView.getClass()
                    .getDeclaredField("mShiftingMode");

            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);

            for (int i = 0; i < menuView.getChildCount(); i++) {

                BottomNavigationItemView item =
                        (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                //To update view, set the checked value again
                item.setChecked(item.getItemData().isChecked());
            }


        } catch (NoSuchFieldException | IllegalAccessException | SecurityException e) {
            e.printStackTrace();

        }

        loadFragment(new HomeFragment()  );


    }




    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
               /* case R.id.action_home:

                    fragment = new HomeFragment();
                    loadFragment(fragment);
                    return true;*/
                case R.id.action_service:

                    /*fragment = new ServiceFragment();
                    loadFragment(fragment);*/
                    return true;
                // break;
                case R.id.action_cityguidde:


                    fragment = new CityGuidesFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.action_salepromo:

                    fragment = new SalePromoFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.action_assistance:

                    fragment = new AssistanceFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.action_account:

                    fragment = new AccountFragment();
                    loadFragment(fragment);
                    return true;
            }
            return false;
        }

    };


    private BottomNavigationView.OnNavigationItemReselectedListener mOnNavigationItemReselectedListener = new BottomNavigationView.OnNavigationItemReselectedListener() {
        @Override
        public void onNavigationItemReselected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                /*case R.id.action_home:

                    fragment = new HomeFragment();
                    loadFragment(fragment);
                    break;*/
                case R.id.action_service:

                    Toast.makeText(getApplicationContext(), "This is Service",
                            Toast.LENGTH_LONG).show();
                    fragment = new ServiceFragment();
                    loadFragment(fragment);
                    break;
                // break;
                case R.id.action_cityguidde:

                    Toast.makeText(getApplicationContext(), "This is City Guide",
                            Toast.LENGTH_LONG).show();
                    break;
                case R.id.action_salepromo:

                    Toast.makeText(getApplicationContext(), "This is Sale Promo",
                            Toast.LENGTH_LONG).show();
                    break;
                case R.id.action_assistance:

                    Toast.makeText(getApplicationContext(), "This is Assistance",
                            Toast.LENGTH_LONG).show();
                    break;
                case R.id.action_account:

                    Toast.makeText(getApplicationContext(), "This is Account",
                            Toast.LENGTH_LONG).show();
                    break;
            }
            //return false;
        }
    };


    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
