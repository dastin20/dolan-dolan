package id.geomedia.apps.ideeverywhere.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.geomedia.apps.ideeverywhere.R;
import id.geomedia.apps.ideeverywhere.activity.Walkthrought;
import id.geomedia.apps.ideeverywhere.adapter.CountryAdapter;
import id.geomedia.apps.ideeverywhere.adapter.ProvinceAdapter;
import id.geomedia.apps.ideeverywhere.application.AppController;
import id.geomedia.apps.ideeverywhere.application.URLs;
import id.geomedia.apps.ideeverywhere.model.CountryModel;
import id.geomedia.apps.ideeverywhere.model.ProvinceModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeDestinationFragment extends Fragment {


    private List<CountryModel> CountryList = new ArrayList<CountryModel>();
    private List<ProvinceModel> ProvinceList = new ArrayList<ProvinceModel>();
    private CountryAdapter adapter;
    private ProvinceAdapter pAdapter;

    private ProgressDialog pDialog;
    private static final String TAG = HomeDestinationFragment.class.getSimpleName();

    public HomeDestinationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_destination, container, false);
        ListView listView = view.findViewById(R.id.listview2);
        adapter = new CountryAdapter(getActivity(), CountryList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String id_param = ((TextView) view.findViewById(R.id.text_id))
                        .getText().toString();
                Bundle bundle = new Bundle();
                bundle.putString("id_country", id_param);
                ProvinceFragment fragment = new ProvinceFragment();
                fragment.setArguments(bundle);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, new ProvinceFragment());

                transaction.addToBackStack(null);
                transaction.commit();
                //loadFragment(new ProvinceFragment()  );

            }
        });
        hideSoftKeyboard();
        loadProducts();
        return view;
    }
    private void loadFragment(Fragment fragment) {
        // load fragment
       FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private void loadProducts() {

        /*
         * Creating a String Request
         * The request type is GET defined by first parameter
         * The URL is defined in the second parameter
         * Then we have a Response Listener and a Error Listener
         * In response listener we will get the JSON response as a String
         * */
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_COUNTRY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //converting the string to json array object
                            JSONArray array = new JSONArray(response);

                            //traversing through all the object
                            for (int i = 0; i < array.length(); i++) {

                                //getting product object from json array
                                JSONObject negara = array.getJSONObject(i);

                                //adding the product to product list
                                CountryModel countRy = new CountryModel();
                                countRy.setId(negara.getInt("id"));
                                //countRy.setTitle(negara.getString("code"));
                                countRy.setTitle(negara.getString("name"));
                                countRy.setFlag(negara.getString("flag"));

                                countRy.setThumbnailUrl(URLs.URL_SERVER_ASSETS_IMG + negara.getString("flag"));
                                // adding movie to movies array
                                CountryList.add(countRy);
                            }

                            //creating adapter object and setting it to recyclerview
                            // CountryAdapter adapter = new CountryAdapter(getChildFragmentManager(), CountryList);
                            //adapter = new CountryAdapter(getActivity(), CountryList);

                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        hidePDialog();
                    }
                });

        //adding our stringrequest to queue
        //Volley.newRequestQueue(getContext()).add(stringRequest);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }


    private void hideSoftKeyboard() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }
}
