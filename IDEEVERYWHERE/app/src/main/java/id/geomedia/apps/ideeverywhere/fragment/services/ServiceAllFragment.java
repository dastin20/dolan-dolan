package id.geomedia.apps.ideeverywhere.fragment.services;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.geomedia.apps.ideeverywhere.R;
import id.geomedia.apps.ideeverywhere.activity.Home;
import id.geomedia.apps.ideeverywhere.activity.Walkthrought;
import id.geomedia.apps.ideeverywhere.activity.resto.RestoActivity;
import id.geomedia.apps.ideeverywhere.activity.resto.RestoDetailActivity;
import id.geomedia.apps.ideeverywhere.adapter.AdapterGridSingleLine;
import id.geomedia.apps.ideeverywhere.adapter.GridAdapter;
import id.geomedia.apps.ideeverywhere.data.DataGenerator;
import id.geomedia.apps.ideeverywhere.fragment.ProvinceFragment;
import id.geomedia.apps.ideeverywhere.util.Tools;
import id.geomedia.apps.ideeverywhere.widget.SpacingItemDecoration;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceAllFragment extends Fragment {
    //ImageView resto;

    Toolbar toolbar;
    CollapsingToolbarLayout collapsingToolbarLayoutAndroid;
    CoordinatorLayout rootLayoutAndroid;
    GridView gridView;
    Context context;
    ArrayList arrayList;


    public static String[] gridViewStrings = {
            "F&B",
            "SPA/Massage",
            "Receptionist",
            "Room Service",
            "House Keeping",
            "Chat Officer",
            "ETC",

    };

    public static int[] gridViewImages = {
            R.drawable.restoa,
            R.drawable.spa,
            R.drawable.receptionist2,
            R.drawable.bellboy,
            R.drawable.housekeeping,
            R.drawable.officer,
            R.drawable.receptionist
    };

    public ServiceAllFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_service_all, container, false);

        /*resto = v.findViewById(R.id.all_image_bg);
        resto.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent i = new Intent(getContext(), RestoActivity.class);
                startActivity(i);
            }
        });*/
        toolbar = v.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        gridView = v.findViewById(R.id.grid);
        gridView.setAdapter(new GridAdapter(getActivity(), gridViewStrings, gridViewImages));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                /*Log.d("ID = " , String.valueOf(position));
                Toast.makeText(getContext(), "" + position,
                        Toast.LENGTH_SHORT).show();*/
                if(position == 0){
                    Intent i = new Intent(getContext(), RestoDetailActivity.class);
                    startActivity(i);
                }
            }
        });
        rootLayoutAndroid = v.findViewById(R.id.android_coordinator_layout);
        collapsingToolbarLayoutAndroid =  v.findViewById(R.id.collapsing_toolbar_android_layout);
        collapsingToolbarLayoutAndroid.setTitle("All service menu");

        return v;
    }

    private void initInstances() {

    }

}
