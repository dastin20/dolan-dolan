package id.geomedia.apps.ideeverywhere.fragment;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import id.geomedia.apps.ideeverywhere.R;
import id.geomedia.apps.ideeverywhere.adapter.AdapterGridSingleLine;
import id.geomedia.apps.ideeverywhere.data.DataGenerator;
import id.geomedia.apps.ideeverywhere.util.Tools;
import id.geomedia.apps.ideeverywhere.widget.SpacingItemDecoration;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomePlacesFragment extends Fragment {

    private View parent_view;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    private RecyclerView recyclerView;
    private AdapterGridSingleLine mAdapter;
    public HomePlacesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home_places, container, false);
        viewPager = v.findViewById(R.id.viewpager_places);
        setupViewPager(viewPager);

        tabLayout = v.findViewById(R.id.tab_places);
        tabLayout.setupWithViewPager(viewPager);

       /* parent_view = v.findViewById(android.R.id.content);

        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.addItemDecoration(new SpacingItemDecoration(2, Tools.dpToPx(getContext(), 3), true));
        recyclerView.setHasFixedSize(true);
        initComponent();*/
        return v;
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        adapter.addFragment(new PlaceEditorChoiceFragment(), "Editor Choice");
        adapter.addFragment(new PlaceFriendRecomendationFragment(), "Friend's Recomendation");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    private void initComponent() {


        List<Integer> items = DataGenerator.getNatureImages(getActivity());
        items.addAll(DataGenerator.getNatureImages(getActivity()));
        items.addAll(DataGenerator.getNatureImages(getActivity()));
        items.addAll(DataGenerator.getNatureImages(getActivity()));

        //set data and list adapter
        mAdapter = new AdapterGridSingleLine(getContext(), items);
        recyclerView.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterGridSingleLine.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Integer obj, int position) {
                Snackbar.make(parent_view, "Item " + position + " clicked", Snackbar.LENGTH_SHORT).show();
            }
        });

    }



}
