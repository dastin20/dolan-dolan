package id.geomedia.apps.ideeverywhere.adapter;

/**
 * Created by Lenovo on 5/16/2018.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.bumptech.glide.Glide;

import java.util.List;

import id.geomedia.apps.ideeverywhere.R;
import id.geomedia.apps.ideeverywhere.application.AppController;
import id.geomedia.apps.ideeverywhere.application.URLs;
import id.geomedia.apps.ideeverywhere.model.CountryModel;

import static android.widget.ImageView.ScaleType.CENTER_CROP;

public class CountryAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<CountryModel> cItems;

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    public CountryAdapter(Activity activity, List<CountryModel> countryItems) {
        this.activity = activity;
        this.cItems = countryItems;
    }

    @Override
    public int getCount() {
        return cItems.size();
    }

    @Override
    public Object getItem(int location) {
        return cItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_home_destination, null);

        TextView id = convertView.findViewById(R.id.text_id);
        //TextView img = convertView.findViewById(R.id.flag_name);
        TextView country = convertView.findViewById(R.id.text_country);
        NetworkImageView images = convertView.findViewById(R.id.image_bg);
        // getting movie data for the row
        CountryModel m = cItems.get(position);
        images.setScaleType(CENTER_CROP);
        images.setImageUrl(m.getThumbnailUrl(), imageLoader);
        id.setText(m.getIdx());
        country.setText(m.getTitle());

        return convertView;
    }

}