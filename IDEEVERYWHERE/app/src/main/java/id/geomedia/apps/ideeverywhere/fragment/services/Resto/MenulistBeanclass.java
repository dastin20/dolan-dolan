package id.geomedia.apps.ideeverywhere.fragment.services.Resto;

/**
 * Created by Rp on 3/30/2016.
 */


//********GRIDVIEW************
public class MenulistBeanclass {
    private int img_m1;
    private String item_name;
    private String description_m1;

    public MenulistBeanclass(int img_m1, String item_name, String description_m1) {
        this.img_m1 = img_m1;
        this.item_name = item_name;
        this.description_m1 = description_m1;
    }

    public int getImg_m1() {
        return img_m1;
    }

    public void setImg_m1(int img_m1) {
        this.img_m1 = img_m1;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getDescription_m1() {
        return description_m1;
    }

    public void setDescription_m1(String description_m1) {
        this.description_m1 = description_m1;
    }
}
