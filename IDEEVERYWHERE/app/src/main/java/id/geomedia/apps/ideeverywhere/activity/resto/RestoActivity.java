package id.geomedia.apps.ideeverywhere.activity.resto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import id.geomedia.apps.ideeverywhere.R;
import id.geomedia.apps.ideeverywhere.activity.Walkthrought;
import id.geomedia.apps.ideeverywhere.fragment.ProvinceFragment;
import id.geomedia.apps.ideeverywhere.fragment.services.Resto.MenulistBeanclass;
import id.geomedia.apps.ideeverywhere.fragment.services.Resto.MenulistGridviewAdapter;

public class RestoActivity extends AppCompatActivity {
    private GridView gridview;
    private ArrayList<MenulistBeanclass> beanclassArrayList;
    private MenulistGridviewAdapter gridviewAdapter;

    private int[] img_m1 = {R.drawable.image1_menu1, R.drawable.image2_menu1, R.drawable.image3_menu1, R.drawable.image4_menu1, R.drawable.image1_menu1, R.drawable.image2_menu1, R.drawable.image3_menu1, R.drawable.image4_menu1};
    private String[] item_name = {"Coffe", "Breakfast", "Munchies", "Sandwiches", "Coffe", "Breakfast", "Munchies", "Sandwiches"};
    private String[] description_m1 = {"Freshly brewed coffee.", "Hearty, hot & full of flavour.", "Perfectly baked goodies.", "Fresh, healthy and tasty.","Freshly brewed coffee.", "Hearty, hot & full of flavour.", "Perfectly baked goodies.", "Fresh, healthy and tasty."};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resto);
        gridview = findViewById(R.id.gridview);
        beanclassArrayList = new ArrayList<MenulistBeanclass>();

        for (int i = 0; i < img_m1.length; i++) {

            MenulistBeanclass beanclass = new MenulistBeanclass(img_m1[i], item_name[i], description_m1[i]);
            beanclassArrayList.add(beanclass);

        }
        gridviewAdapter = new MenulistGridviewAdapter(RestoActivity.this, beanclassArrayList);

        gridview.setAdapter(gridviewAdapter);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent myIntent = new Intent(RestoActivity.this, RestoDetailActivity.class);
               startActivity(myIntent);

            }
        });
    }
}
