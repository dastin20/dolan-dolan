package id.geomedia.apps.ideeverywhere.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.geomedia.apps.ideeverywhere.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SalePromoFragment extends Fragment {


    public SalePromoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sale_promo, container, false);
    }

}
