package id.geomedia.apps.ideeverywhere.model;

/**
 * Created by Lenovo on 5/16/2018.
 */

public class FoodModel {
    private Integer id;
    private String idx,name,price,time;
    private String img_flag,thumbnailUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer Id) {
        this.id = Id;
    }

    public String getTitle() {
        return name;
    }

    public void setTitle(String name) {
        this.name = name;
    }

    public void setIdx(String idxs) {
        this.idx = idxs;
    }
    public String getIdx() {
        return idx;
    }


    public String getFlag() {
        return img_flag;
    }

    public void setFlag(String Flag) {
        this.img_flag = Flag;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTimeS() {
        return time;
    }

    public void setTimeS(String time) {
        this.time = time;
    }
}