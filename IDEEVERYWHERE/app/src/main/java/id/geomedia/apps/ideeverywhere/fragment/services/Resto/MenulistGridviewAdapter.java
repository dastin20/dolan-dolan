package id.geomedia.apps.ideeverywhere.fragment.services.Resto;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;

import id.geomedia.apps.ideeverywhere.R;


public class MenulistGridviewAdapter extends BaseAdapter {

    Context context;

    ArrayList<MenulistBeanclass> bean;
    Typeface fonts1, fonts2;


    public MenulistGridviewAdapter(Context context, ArrayList<MenulistBeanclass> bean) {
        this.bean = bean;
        this.context = context;
    }

    @Override
    public int getCount() {
        return bean.size();
    }

    @Override
    public Object getItem(int position) {
        return bean.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.gridview_menu_list, null);

            viewHolder = new ViewHolder();

            viewHolder.img_m1 = (ImageView) convertView.findViewById(R.id.img_m1);
            viewHolder.item_name = (TextView) convertView.findViewById(R.id.item_name);
            viewHolder.description_m1 = (TextView) convertView.findViewById(R.id.description_m1);


            convertView.setTag(viewHolder);


        } else {

            viewHolder = (ViewHolder) convertView.getTag();
        }


        MenulistBeanclass bean = (MenulistBeanclass) getItem(position);

        viewHolder.img_m1.setImageResource(bean.getImg_m1());
        viewHolder.item_name.setText(bean.getItem_name());
        viewHolder.description_m1.setText(bean.getDescription_m1());


        return convertView;
    }

    private class ViewHolder {
        ImageView img_m1;
        TextView item_name;
        TextView description_m1;


    }
}


