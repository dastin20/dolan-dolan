package id.geomedia.apps.ideeverywhere.model;

/**
 * Created by Lenovo on 5/16/2018.
 */

public class ProvinceModel {
    private Integer id;
    private String name,code;

    public Integer getId() {
        return id;
    }

    public void setId(Integer Id) {
        this.id = Id;
    }

    public String getTitle() {
        return name;
    }

    public void setTitle(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }



}