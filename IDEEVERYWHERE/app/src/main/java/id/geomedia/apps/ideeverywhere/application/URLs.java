package id.geomedia.apps.ideeverywhere.application;

/**
 * Created by Lenovo on 5/8/2018.
 */

public class URLs {
     public static String URL_SERVER = "http://ideeverywhere.com/ide/api/";
    public static String URL_SERVER_ASSETS_IMG = "http://ideeverywhere.com/ide/api/img/";
    public static String ASSETS_IMG_ACCOMODATION = "http://ideeverywhere.com/ide/api/img/Bali/Accommodation/";
    public static String ASSETS_IMG_ATTRACTION = "http://ideeverywhere.com/ide/api/img/Bali/Attraction/";
    public static String ASSETS_IMG_DIVSPOT = "http://ideeverywhere.com/ide/api/img/Bali/Divingspot/";

    public static String URL_COUNTRY = URL_SERVER + "country.php";
    public static String URL_PROVINCE = URL_SERVER + "province.php";
    public static String URL_CITY = URL_SERVER + "city.php";
    public static String URL_LOGIN = URL_SERVER + "login.php";
    public static String URL_SERVICE_CAT = URL_SERVER + "category_service.php";
    public static String URL_SERVICE_HOTEL = URL_SERVER + "service_hotel.php";
    public static String URL_IMAGE_SLIDER = URL_SERVER + "image_home.php";
    public static String URL_IMAGE_SLIDER_ATTRACTION = URL_SERVER + "image_attraction.php";
    public static String URL_IMAGE_SLIDER_DIVING = URL_SERVER + "image_diving.php";


}