package id.geomedia.apps.ideeverywhere.fragment.services.Resto;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;


import java.util.ArrayList;

import id.geomedia.apps.ideeverywhere.R;


public class Menu2ListviewAdapter extends BaseAdapter {

    Context context;

    ArrayList<Menu2BeanclassList> bean;
    Typeface fonts1,fonts2;
    RatingBar ratingbar;
    Activity main;




    public Menu2ListviewAdapter(Activity activity, Context context, ArrayList<Menu2BeanclassList> bean) {

        this.main = activity;
        this.context = context;
        this.bean = bean;
    }

    @Override
    public int getCount() {
        return bean.size();
    }

    @Override
    public Object getItem(int position) {
        return bean.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.listview_menu2,null);

            viewHolder = new ViewHolder();

            viewHolder.image_m2 = (ImageView) convertView.findViewById(R.id.image_m2);
            viewHolder.item_name_m2 = (TextView) convertView.findViewById(R.id.item_name_m2);
            viewHolder.text_m2 = (TextView) convertView.findViewById(R.id.text_m2);
            viewHolder.price_m2 = (TextView) convertView.findViewById(R.id.price_m2);
            viewHolder.ratingtext_m2 = (TextView) convertView.findViewById(R.id.ratingtext_m2);








            convertView.setTag(viewHolder);


        }else {

            viewHolder = (ViewHolder)convertView.getTag();
        }







        Menu2BeanclassList bean = (Menu2BeanclassList)getItem(position);

        viewHolder.image_m2.setImageResource(bean.getImage_m2());
        viewHolder.item_name_m2.setText(bean.getItem_name_m2());
        viewHolder.text_m2.setText(bean.getText_m2());
        viewHolder.price_m2.setText(bean.getPrice_m2());
        viewHolder.ratingtext_m2.setText(bean.getRatingtext_m2());




        return convertView;
    }

    private class ViewHolder {
        ImageView image_m2;
        TextView item_name_m2;
        TextView text_m2;
        TextView price_m2;
        TextView discount;
        TextView ratingtext_m2;



    }
}



