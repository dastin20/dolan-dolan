package id.geomedia.apps.ideeverywhere.fragment.services.Resto;

/**
 * Created by apple on 15/03/16.
 *
 * //********LISTVIEW************
 */
public class Menu2BeanclassList {

    private int image_m2;
    private String item_name_m2;
    private String text_m2;
    private String price_m2;
    private String ratingtext_m2;


    public Menu2BeanclassList(int image_m2, String item_name_m2, String text_m2, String price_m2, String ratingtext_m2) {
        this.image_m2 = image_m2;
        this.item_name_m2 = item_name_m2;
        this.text_m2 = text_m2;
        this.price_m2 = price_m2;
        this.ratingtext_m2 = ratingtext_m2;
    }

    public int getImage_m2() {
        return image_m2;
    }

    public void setImage_m2(int image_m2) {
        this.image_m2 = image_m2;
    }

    public String getItem_name_m2() {
        return item_name_m2;
    }

    public void setItem_name_m2(String item_name_m2) {
        this.item_name_m2 = item_name_m2;
    }

    public String getText_m2() {
        return text_m2;
    }

    public void setText_m2(String text_m2) {
        this.text_m2 = text_m2;
    }

    public String getPrice_m2() {
        return price_m2;
    }

    public void setPrice_m2(String price_m2) {
        this.price_m2 = price_m2;
    }

    public String getRatingtext_m2() {
        return ratingtext_m2;
    }

    public void setRatingtext_m2(String ratingtext_m2) {
        this.ratingtext_m2 = ratingtext_m2;
    }
}
