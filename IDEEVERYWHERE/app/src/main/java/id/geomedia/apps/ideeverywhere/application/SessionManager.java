package id.geomedia.apps.ideeverywhere.application;

/**
 * Created by Lenovo on 5/8/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import id.geomedia.apps.ideeverywhere.activity.LoginActivity;


public class SessionManager {
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();
    //private static Context mCtx;
    // Shared Preferences
    SharedPreferences pref;

    Editor editor;
    Context _context;


    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "AlifLogin";

    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";

    public static final String SHARED_PREF_ID_HOTEL = "id";
    public static final String SHARED_PREF_NAME_HOTEL = "name_hotel";
    public static final String SHARED_PREF_ROOM_HOTEL = "room";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn) {

        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public void setHotel(String id,String name,String room) {

        editor.putString(SHARED_PREF_ID_HOTEL, id);
        editor.putString(SHARED_PREF_NAME_HOTEL, name);
        editor.putString(SHARED_PREF_ROOM_HOTEL, room);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    //this method will logout the user
    public void logout() {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        _context.startActivity(new Intent(_context, LoginActivity.class));
    }
}
