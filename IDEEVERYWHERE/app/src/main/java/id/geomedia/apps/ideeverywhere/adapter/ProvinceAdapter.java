package id.geomedia.apps.ideeverywhere.adapter;

/**
 * Created by Lenovo on 5/16/2018.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import id.geomedia.apps.ideeverywhere.R;
import id.geomedia.apps.ideeverywhere.application.AppController;
import id.geomedia.apps.ideeverywhere.model.CountryModel;
import id.geomedia.apps.ideeverywhere.model.ProvinceModel;

import static android.widget.ImageView.ScaleType.CENTER_CROP;

public class ProvinceAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<ProvinceModel> cItems;

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    public ProvinceAdapter(Activity activity, List<ProvinceModel> provinceItems) {
        this.activity = activity;
        this.cItems = provinceItems;
    }

    @Override
    public int getCount() {
        return cItems.size();
    }

    @Override
    public Object getItem(int location) {
        return cItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_province, null);

        TextView prov = convertView.findViewById(R.id.text_province);

        ProvinceModel m = cItems.get(position);
        prov.setText(m.getTitle());

        return convertView;
    }

}