package id.geomedia.apps.ideeverywhere.fragment.services.Resto;

/**
 * Created by Rp on 3/30/2016.
 */


//********GRIDVIEW************
public class MenuListGrid {
    private int img_m1;
    private String item_name;

    public MenuListGrid(int img_m1, String item_name) {
        this.img_m1 = img_m1;
        this.item_name = item_name;
    }

    public int getImg_m1() {
        return img_m1;
    }

    public void setImg_m1(int img_m1) {
        this.img_m1 = img_m1;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

}
