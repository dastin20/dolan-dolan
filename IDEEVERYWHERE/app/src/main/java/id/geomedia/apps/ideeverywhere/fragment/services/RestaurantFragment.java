package id.geomedia.apps.ideeverywhere.fragment.services;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.ArrayList;

import id.geomedia.apps.ideeverywhere.R;
import id.geomedia.apps.ideeverywhere.fragment.services.Resto.MenulistBeanclass;
import id.geomedia.apps.ideeverywhere.fragment.services.Resto.MenulistGridviewAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class RestaurantFragment extends Fragment {


    public RestaurantFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_restaurant, container, false);

        return v;
    }

}
